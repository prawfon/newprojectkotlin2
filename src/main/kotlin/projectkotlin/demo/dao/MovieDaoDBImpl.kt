package projectkotlin.demo.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import projectkotlin.demo.entity.Movies
import projectkotlin.demo.repository.MovieRepository

@Profile("db")
@Repository
class MovieDaoDBImpl:MoviesDao {
    override fun getMovieByMovieName(name: String): List<Movies> {
        return  movieRepository.findBymovieNameContainingIgnoreCase(name)
    }

    override fun getMovies(): List<Movies> {
        return movieRepository.findAll().filterIsInstance(Movies::class.java)
    }

    @Autowired
    lateinit var movieRepository: MovieRepository
}