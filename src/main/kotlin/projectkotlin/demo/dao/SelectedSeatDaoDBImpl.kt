package projectkotlin.demo.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import projectkotlin.demo.entity.SelectedSeat
import projectkotlin.demo.repository.SelectedSeatRespository

@Profile("db")
@Repository
class SelectedSeatDaoDBImpl:SelectedSeatDao{
    override fun getSelectedSeats(): List<SelectedSeat> {
        return selectedSeatRespository.findAll().filterIsInstance(SelectedSeat::class.java)
    }
    @Autowired
    lateinit var selectedSeatRespository: SelectedSeatRespository
}
