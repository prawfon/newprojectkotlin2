package projectkotlin.demo.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import projectkotlin.demo.entity.Subtitle
import projectkotlin.demo.repository.SubtitleRepository

@Profile("db")
@Repository
class SubtitleDaoDBImpl:SubtitleDao{

    override fun getSubtitle(): List<Subtitle> {
        return subtitleRepository.findAll().filterIsInstance(Subtitle::class.java)
    }
    @Autowired
    lateinit var subtitleRepository: SubtitleRepository
}