package projectkotlin.demo.dao

import projectkotlin.demo.entity.Movies


interface MoviesDao{
   fun getMovies():List<Movies>
   fun getMovieByMovieName(name: String): List<Movies>
}