package projectkotlin.demo.dao


import org.springframework.stereotype.Repository
import projectkotlin.demo.entity.Customer


interface CustomerDao{
    fun getCustomers(): List<Customer>
    fun getCustomerByName(name: String): List<Customer>
    fun getCustomerByfirstname(name: String): List<Customer>
    fun getCustomerBylastname(name: String): List<Customer>
    fun save(customer: Customer): Customer
}