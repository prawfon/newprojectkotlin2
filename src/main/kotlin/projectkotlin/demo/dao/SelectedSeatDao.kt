package projectkotlin.demo.dao

import projectkotlin.demo.entity.SelectedSeat

interface SelectedSeatDao{
    fun getSelectedSeats(): List<SelectedSeat>
}