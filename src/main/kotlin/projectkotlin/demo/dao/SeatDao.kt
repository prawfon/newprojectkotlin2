package projectkotlin.demo.dao

import projectkotlin.demo.entity.Seats

interface SeatDao{
    fun getSeats():List<Seats>
}
