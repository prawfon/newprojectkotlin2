package projectkotlin.demo.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import projectkotlin.demo.entity.Customer
import projectkotlin.demo.repository.CustomerRepository


@Profile ("db")
@Repository
class CustomerDaoImpl: CustomerDao {
    @Autowired
    lateinit var customerRepository: CustomerRepository
    override fun save(customer: Customer): Customer {
        return customerRepository.save(customer)
    }

    override fun getCustomerBylastname(name: String): List<Customer> {
        return  customerRepository.findBylastNameContainingIgnoreCase(name)
    }

    override fun getCustomerByfirstname(name: String): List<Customer> {
        return customerRepository.findByfirstNameContainingIgnoreCase(name)
    }

    override fun getCustomerByName(name: String): List<Customer> {
        return customerRepository.findByuserNameContainingIgnoreCase(name)
    }

    override fun getCustomers(): List<Customer> {
        return customerRepository.findAll().filterIsInstance(Customer::class.java)
    }


}

