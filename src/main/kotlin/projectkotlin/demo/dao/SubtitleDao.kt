package projectkotlin.demo.dao

import projectkotlin.demo.entity.Subtitle

interface SubtitleDao{
    fun getSubtitle():List<Subtitle>
}