package projectkotlin.demo.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import projectkotlin.demo.entity.Showtime
import projectkotlin.demo.repository.ShowtimeRepository

@Profile("db")
@Repository
class ShowtimeDaoImpl : ShowtimeDao {
    override fun findById(showTimeId: Long?): Showtime {
        return showtimeRepository.findById(showTimeId!!).orElse(null)
    }

    override fun getShowtimeByMoviename(name: String): List<Showtime> {
        return showtimeRepository.findBymovies_MovieNameContainingIgnoreCase(name)
    }

    override fun getShowtimeByCinemaname(name: String): List<Showtime> {
        return showtimeRepository.findBycinema_nameContainingIgnoreCase(name)
    }

    @Autowired
    lateinit var showtimeRepository: ShowtimeRepository

    override fun getShowtimes(): List<Showtime> {
        return showtimeRepository.findAll().filterIsInstance(Showtime::class.java)
    }
}