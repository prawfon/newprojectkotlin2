package projectkotlin.demo.dao

import projectkotlin.demo.entity.SeatInShowTime

interface SeatInShowTimeDao {
    fun findById(id: Long?): SeatInShowTime
    fun save(seat: SeatInShowTime): SeatInShowTime

}