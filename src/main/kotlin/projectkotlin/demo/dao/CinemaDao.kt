package projectkotlin.demo.dao


import org.springframework.stereotype.Repository
import projectkotlin.demo.entity.Cinema


interface CinemaDao{
    fun getAllCinema(): List<Cinema>
    fun getCinemaByName(name: String): List<Cinema>
}

