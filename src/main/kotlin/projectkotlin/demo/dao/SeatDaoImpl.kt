package projectkotlin.demo.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import projectkotlin.demo.entity.Seats
import projectkotlin.demo.repository.SeatRepository

@Profile("db")
@Repository
class SeatDaoImpl:SeatDao{
    @Autowired
    lateinit var seatRepository: SeatRepository
    override fun getSeats(): List<Seats> {
        return seatRepository.findAll().filterIsInstance(Seats::class.java)
    }

}
