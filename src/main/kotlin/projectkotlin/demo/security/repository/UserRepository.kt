package projectkotlin.demo.security.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.demo.security.entity.JwtUser

interface UserRepository: CrudRepository<JwtUser, Long> {
    fun findByUsername(username: String): JwtUser
}