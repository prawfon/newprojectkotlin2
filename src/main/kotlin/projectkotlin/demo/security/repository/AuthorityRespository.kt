package projectkotlin.demo.security.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.demo.security.entity.Authority
import projectkotlin.demo.security.entity.AuthorityName


interface AuthorityRepository: CrudRepository<Authority, Long> {
    fun findByName(input: AuthorityName): Authority
}