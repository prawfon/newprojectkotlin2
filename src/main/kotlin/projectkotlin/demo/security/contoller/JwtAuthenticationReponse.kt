package projectkotlin.demo.security.contoller

data class JwtAuthenticationResponse(
        var token: String? = null
)