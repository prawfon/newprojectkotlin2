package projectkotlin.demo.service

import projectkotlin.demo.entity.Customer

interface CustomerService {
    fun getCustomers(): List<Customer>
    fun getCustomerByuserName(name: String): List<Customer>
    fun getCustomerByfirstName(name: String): List<Customer>
    fun getCustomerBylastName(name: String): List<Customer>
    fun save(customer: Customer): Customer
}