package projectkotlin.demo.service

import projectkotlin.demo.entity.Soundtrack


interface SoundtrackService{
    fun getSoundtrack():List<Soundtrack>
}
