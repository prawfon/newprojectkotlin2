package projectkotlin.demo.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import projectkotlin.demo.dao.SoundtrackDao
import projectkotlin.demo.entity.Soundtrack

@Service
class SoundtrackServiceImpl:SoundtrackService{
    @Autowired
    lateinit var soundtrackDao: SoundtrackDao

    override fun getSoundtrack(): List<Soundtrack> {
        return soundtrackDao.getSoundtrack()
    }
}
