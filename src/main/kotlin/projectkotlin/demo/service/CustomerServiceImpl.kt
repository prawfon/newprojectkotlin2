package projectkotlin.demo.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import projectkotlin.demo.dao.CustomerDao
import projectkotlin.demo.entity.Customer

@Service
class CustomerServiceIpml: CustomerService{
    override fun save(customer: Customer): Customer {
        return customerDao.save(customer)
    }

    override fun getCustomerBylastName(name: String): List<Customer> {
        return customerDao.getCustomerBylastname(name)
    }

    override fun getCustomerByfirstName(name: String): List<Customer> {
        return customerDao.getCustomerByfirstname(name)
    }


    override fun getCustomerByuserName(name: String): List<Customer> {
        return customerDao.getCustomerByName(name)
    }

    @Autowired
    lateinit var customerDao : CustomerDao

    override fun getCustomers(): List<Customer> {
        return customerDao.getCustomers()
    }
}