package projectkotlin.demo.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import projectkotlin.demo.dao.SeatDao
import projectkotlin.demo.entity.Seats

@Service
class SeatServiceImpl:SeatService{
    @Autowired
    lateinit var seatsDao: SeatDao
    override fun getSeatService(): List<Seats> {
        return seatsDao.getSeats()
    }

}

