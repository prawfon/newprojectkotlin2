package projectkotlin.demo.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import projectkotlin.demo.dao.CinemaDao
import projectkotlin.demo.entity.Cinema

@Service
class CinemaServiceImpl:CinemaService{
    override fun getCinemaByName(name: String): List<Cinema> {
        return cinemaDao.getCinemaByName(name)
    }

    @Autowired
    lateinit var cinemaDao: CinemaDao
    override fun getCinema(): List<Cinema>{
        return cinemaDao.getAllCinema()
    }
}