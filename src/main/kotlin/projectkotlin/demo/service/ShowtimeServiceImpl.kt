package projectkotlin.demo.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import projectkotlin.demo.dao.ShowtimeDao
import projectkotlin.demo.entity.Showtime
import projectkotlin.demo.util.MapperUtil

@Service
class ShowtimeServiceImpl:ShowtimeService{
    override fun getShowtimeByMovieName(name: String): List<Showtime> {
        return showtimeDao.getShowtimeByMoviename(name)
    }

    override fun getShowtimeByCinemaName(name: String): List<Showtime> {
        return showtimeDao.getShowtimeByCinemaname(name)
    }

    @Autowired
    lateinit var showtimeDao: ShowtimeDao

    override fun getShowtimes(): List<Showtime> {
        return showtimeDao.getShowtimes()
    }
}