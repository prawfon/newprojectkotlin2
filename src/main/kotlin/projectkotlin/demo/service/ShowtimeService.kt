package projectkotlin.demo.service

import projectkotlin.demo.entity.Showtime

interface ShowtimeService {
    fun getShowtimes(): List<Showtime>
    fun getShowtimeByCinemaName(name: String): List<Showtime>
    fun getShowtimeByMovieName(name: String): List<Showtime>
}