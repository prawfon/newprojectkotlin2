package projectkotlin.demo.service

import projectkotlin.demo.entity.Booking
interface BookingService{
    fun save(booking: Booking): Booking
}