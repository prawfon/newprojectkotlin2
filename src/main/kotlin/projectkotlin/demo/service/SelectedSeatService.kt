package projectkotlin.demo.service

import projectkotlin.demo.entity.SelectedSeat

interface SelectedSeatService{
    fun getSelectedSeats(): List<SelectedSeat>
//    fun getSelectedSeatByVertical(vertical: String): List<SelectedSeat>
//    fun getSelectedSeatByHorizontal(horizontal: String): List<SelectedSeat>
}