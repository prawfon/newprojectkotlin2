package projectkotlin.demo.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

import projectkotlin.demo.service.ShowtimeService
import projectkotlin.demo.util.MapperUtil


@RestController
class ShowtimeController{
    @Autowired
    lateinit var showtimeService: ShowtimeService
    @GetMapping("/showtime")
    fun getAllShowtime(): ResponseEntity<Any> {
        val showtimes = showtimeService.getShowtimes()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapShowtimeDto(showtimes))
    }

    @GetMapping("/showtime/{cinemaName}")
    fun getShowtimeByCinemaName(@RequestParam("cinemaName") name: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapShowtimeDto(showtimeService.getShowtimeByCinemaName(name))
        output?.let { return ResponseEntity.ok(output) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/showtime/{movieName}")
    fun getShowtimeByMovieName(@RequestParam("movieName") name: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapShowtimeDto(showtimeService.getShowtimeByMovieName(name))
        output?.let { return ResponseEntity.ok(output) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
}
