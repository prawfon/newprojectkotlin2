package projectkotlin.demo.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import projectkotlin.demo.service.SoundtrackService
import projectkotlin.demo.util.MapperUtil

@RestController
class SoundtrackController{
    @Autowired
    lateinit var soundtrackService: SoundtrackService

    @GetMapping("/soundtrack")
    fun getAllSoundtrack(): ResponseEntity<Any> {
        val soundtracks = soundtrackService.getSoundtrack()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapSoundtrackDto(soundtracks))
    }
}
