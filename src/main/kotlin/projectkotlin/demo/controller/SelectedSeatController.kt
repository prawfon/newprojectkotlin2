package projectkotlin.demo.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import projectkotlin.demo.service.SelectedSeatService
import projectkotlin.demo.util.MapperUtil

@RestController
class SelectedSeatController {
    @Autowired
    lateinit var selectedSeatService: SelectedSeatService

    @GetMapping("/selectedSeat")
    fun getAllSelectedSeat(): ResponseEntity<Any> {
        val selectedSeats = selectedSeatService.getSelectedSeats()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapSelectedSeatDto(selectedSeats))
    }
}