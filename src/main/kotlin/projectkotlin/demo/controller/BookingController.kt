package projectkotlin.demo.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import projectkotlin.demo.entity.Booking
import projectkotlin.demo.service.BookingService

@RestController
class BookingController{
    @Autowired
    lateinit var bookingService: BookingService

    @PostMapping("/booking")
    fun addBooking(@RequestBody booking: Booking): ResponseEntity<Any> {
        val bookings = bookingService.save(booking)
        return ResponseEntity.ok(bookings)
    }
}
