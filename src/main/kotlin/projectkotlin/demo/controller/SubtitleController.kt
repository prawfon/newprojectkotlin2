package projectkotlin.demo.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

import projectkotlin.demo.service.SubtitleService
import projectkotlin.demo.util.MapperUtil

@RestController
class SubtitleController{
    @Autowired
    lateinit var subtitleService: SubtitleService

    @GetMapping("/subtitle")
    fun getAllSubtitle(): ResponseEntity<Any> {
        val subtitles = subtitleService.getSubtitle()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapSubtitleDto(subtitles))
    }
}
