package projectkotlin.demo.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

import projectkotlin.demo.service.SeatService
import projectkotlin.demo.util.MapperUtil

@RestController
class SeatController{
    @Autowired
    lateinit var seatService: SeatService
    @GetMapping("/seat")
    fun getAllSeat(): ResponseEntity<Any> {
        val seats = seatService.getSeatService()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapSeatDto(seats))
    }

}
