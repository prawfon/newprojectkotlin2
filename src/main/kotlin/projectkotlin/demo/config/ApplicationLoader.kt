package projectkotlin.demo.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import projectkotlin.demo.entity.*
import projectkotlin.demo.repository.*
import projectkotlin.demo.security.entity.Authority
import projectkotlin.demo.security.entity.AuthorityName
import projectkotlin.demo.security.entity.JwtUser
import projectkotlin.demo.security.repository.AuthorityRepository
import projectkotlin.demo.security.repository.UserRepository
import java.sql.Timestamp
import javax.transaction.Transactional

@Component
class ApplicationLoader : ApplicationRunner {
    @Autowired
    lateinit var seatRepository: SeatRepository
    @Autowired
    lateinit var cinemaRepository: CinemaRepository
    @Autowired
    lateinit var soundtrackRespository: SoundtrackRespository
    @Autowired
    lateinit var subtitleRepository: SubtitleRepository
    @Autowired
    lateinit var movieRepository: MovieRepository
    @Autowired
    lateinit var showtimeRepository: ShowtimeRepository
    @Autowired
    lateinit var selectedSeatRespository: SelectedSeatRespository
    @Autowired
    lateinit var customerRepository: CustomerRepository
    @Autowired
    lateinit var seatInShowTimeRepository: SeatInShowTimeRepository

    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var userRepository: UserRepository
    @Transactional
    fun loadUsernameAndPassword() {
        val auth1 = Authority(name = AuthorityName.ROLE_ADMIN)
        val auth2 = Authority(name = AuthorityName.ROLE_CUSTOMER)
        val auth3 = Authority(name = AuthorityName.ROLE_GENERAL)
        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)
        val encoder = BCryptPasswordEncoder()
        val cust1 = Customer(userName = "สมชาติ", email = "a@b.com")
        val custJwt = JwtUser(
                username = "customer",
                password = encoder.encode("password"),
                email = cust1.email,
                enabled = true,
                firstname = cust1.userName,
                lastname = "unknown"
        )
        customerRepository.save(cust1)
        userRepository.save(custJwt)
        cust1.jwtUser = custJwt
        custJwt.user = cust1
        custJwt.authorities.add(auth2)
        custJwt.authorities.add(auth3)

        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)
    }



    @Transactional
    override fun run(args: ApplicationArguments?) {
        var movie1 = movieRepository.save(Movies("อลิตา แบทเทิล แองเจิ้ล", 125
                , "https://lh3.googleusercontent.com/m-KIGZSYMapNnCIGB3QVXPlqkv7Vu_-VoEXqdleoNZ-CDRVdprMcoUanKbMHeFjlXk38FYPxW0Gar0XEQYI=w1024"))
        var movie2 = movieRepository.save(Movies("อภินิหารไวกิ้งพิชิตมังกร 3", 105
                , "https://lh3.googleusercontent.com/xTpxPyQOfsnpA2lkp8Fz7LjVA-hsadGF0U7c5rwJq4ikRG_np-5_kO-gzHMVkur2Y5BnLQ4Sfki_Qj7T0Mlisw=w1024"))
        var movie3 = movieRepository.save(Movies("Captain Marvel", 130
                , "https://lh3.googleusercontent.com/3Ost-FvXYMVAs84hLxJAn9qqIh9s_YCC8oCySCuOWIJWIu8zPIuKYJ9FG_FEFBqKZoQnguVOuNAkg9kArkPM=w1024"))
        var movie4 = movieRepository.save(Movies("เฟรนด์โซน ระวัง..สิ้นสุดทางเพื่อน", 120
                , "https://lh3.googleusercontent.com/hGX4-13iqHM1Kx_nyj67AtCsFXAjJyS0wxoEcJynOs99BUJXQWLIUQFcsje3jdMW75wtrJU2ktywEF3gxoKe=w1024"))
        var movie5 = movieRepository.save(Movies("สุขสันต์วันตาย 2U", 100
                , "https://lh3.googleusercontent.com/8hKivfDWOC2G8rQolX9850yiPWs5dJzSgyGirJFjU66jeUPV9-5gkm1soLxlHubI5_V_lK1T0vdfinsUlVZtOA=w1024"))
        var movie6 = movieRepository.save(Movies("คุซามะ อินฟินิตี้", 80
                , "https://lh3.googleusercontent.com/Fs3-9D0HjOaP-BWhOhVp-gZAqN3aRyyB5Z7W59f67v_frABpaljFUhFR9Pjs-fTN37jz1mqml9LRUK1tVjjFBg=w1024"))


        val customer1 = customerRepository.save(Customer("Wattana", "123456", "Hotzaymoon@gmail.com", "Wattana", "Potisarach","https://s3.ap-southeast-1.amazonaws.com/camt-dv/1553448502841-gdragon.jpg"))
        val customer2 = customerRepository.save(Customer("Prawfon", "111122", "Prawfon@hotmai.com", "Prawfon", "Thitimatiyakul"))
        val customer3 = customerRepository.save(Customer("Wisarut", "3345678", "Pan1124jooly@gmail.com", "Wisarut", "Chandutee"))
        val customer4 = customerRepository.save(Customer("Surakanya", "888844", "Applepota24@gmail.com", "Surakanya", "Praisuwan"))

        var soundtrack1 = soundtrackRespository.save(Soundtrack("EN"))
        var soundtrack2 = soundtrackRespository.save(Soundtrack("TH"))

        var subtitle1 = subtitleRepository.save(Subtitle("EN"))
        var subtitle2 = subtitleRepository.save(Subtitle("TH"))

        var cinema1 = cinemaRepository.save(Cinema("Cinema1"))
        var cinema2 = cinemaRepository.save(Cinema("Cinema2"))
        var cinema3 = cinemaRepository.save(Cinema("Cinema3"))

        var premium = seatRepository.save(Seats("Premium", 4, 20, SeatType.PREMIUM, 150.00))
        var deluxe = seatRepository.save(Seats("Deluxe", 15, 20, SeatType.DELUXE, 190.00))
        var sofa = seatRepository.save(Seats("Sofa Sweet (Pair)", 20, 20, SeatType.SOFA, 700.00))



        cinema1.seats = mutableListOf(deluxe, premium, sofa)
        cinema2.seats = mutableListOf(deluxe, premium)
        cinema3.seats = mutableListOf(deluxe, premium, sofa)

//        val selectedSeat1 = selectedSeatRespository.save(SelectedSeat("1","5",false))
//        val selectedSeat2 = selectedSeatRespository.save(SelectedSeat("2","8",false))
//        val selectedSeat3 = selectedSeatRespository.save(SelectedSeat("3","10",false))

//        var showtime1 = showtimeRepository.save(Showtime(Timestamp(1553325300).time, Timestamp(1553332500).time))
//        showtime1.movies = movie1
//        showtime1.cinema = cinema1


//===========================================Seats in Cinema and Showtime 1==========================================================
        val showtime1 = showtimeRepository.save(Showtime(1553490000000, 1553497200000))
        showtime1.movies = movie1
        showtime1.cinema = cinema1
        //time1.selectedSeats.add(selectedSeat1)

        var row: Char = 'A'
        for ((index, item) in showtime1.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRespository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.colum!!) {
                    var seat: SeatInShowTime
                    if (item.seatstype === SeatType.SOFA) {
                        seat = seatInShowTimeRepository.save(SeatInShowTime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInShowTimeRepository.save(SeatInShowTime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime1.selectedSeats.add(selectedSeat)
            if (item.seatstype === SeatType.SOFA) {
                row = 'A'
            }
        }
//===========================================Seats in Cinema and Showtime 2==========================================================

        val showtime2 = showtimeRepository.save(Showtime(1553576400000, 1553583600000))
        showtime2.movies = movie2
        showtime2.cinema = cinema2
        //time2.selectedSeats.add(selectedSeat2)
        row = 'A'
        for ((index, item) in showtime2.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRespository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.colum!!) {
                    var seat: SeatInShowTime
                    if (item.seatstype === SeatType.SOFA) {
                        seat = seatInShowTimeRepository.save(SeatInShowTime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInShowTimeRepository.save(SeatInShowTime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime2.selectedSeats.add(selectedSeat)
            if (item.seatstype === SeatType.SOFA) {
                row = 'A'
            }
        }


//===========================================Seats in Cinema and Showtime 3==========================================================

        val showtime3 = showtimeRepository.save(Showtime(1553662800000, 1553670000000))
        showtime3.movies = movie3
        showtime3.cinema = cinema3
        //time3.selectedSeats.add(selectedSeat3)
        row = 'A'
        for ((index, item) in showtime3.cinema!!.seats.withIndex()) {
            var selectedSeat = selectedSeatRespository.save(SelectedSeat(item))
            for (indexR in 1..selectedSeat.seatDetail!!.row!!) {
                for (indexC in 1..selectedSeat.seatDetail!!.colum!!) {
                    var seat: SeatInShowTime
                    if (item.seatstype === SeatType.SOFA) {
                        seat = seatInShowTimeRepository.save(SeatInShowTime(false, "${row}${row}", "C${indexC}"))
                    } else {
                        seat = seatInShowTimeRepository.save(SeatInShowTime(false, "${row}", "C${indexC}"))
                    }
                    selectedSeat.seats.add(seat)
                }
                row++
            }
            showtime3.selectedSeats.add(selectedSeat)
            if (item.seatstype === SeatType.SOFA) {
                row = 'A'
            }
        }


        movie1.soundtrack = mutableListOf(soundtrack1, soundtrack2)
        movie1.subtitle = mutableListOf(subtitle1, subtitle2)
        movie2.soundtrack = mutableListOf(soundtrack1, soundtrack2)
        movie2.subtitle = mutableListOf(subtitle1, subtitle2)
        movie3.soundtrack = mutableListOf(soundtrack1, soundtrack2)
        movie3.subtitle = mutableListOf(subtitle1, subtitle2)
        movie4.soundtrack = mutableListOf(soundtrack1, soundtrack2)
        movie4.subtitle = mutableListOf(subtitle1, subtitle2)
        movie5.soundtrack = mutableListOf(soundtrack1, soundtrack2)
        movie5.subtitle = mutableListOf(subtitle1, subtitle2)
        movie6.soundtrack = mutableListOf(soundtrack1, soundtrack2)
        movie6.subtitle = mutableListOf(subtitle1, subtitle2)

        loadUsernameAndPassword()

//        var showtime2 = showtimeRepository.save(Showtime(Timestamp(1553425200).time, Timestamp(1553428800).time))
//        var showtime3 = showtimeRepository.save(Showtime(Timestamp(1551792600).time, Timestamp(1551799800).time))

//        showtime1.selectedSeats.add(selectedSeat1)

//        showtime2.movies = movie2
//        showtime2.cinema = cinema2
//        showtime2.selectedSeats.add(selectedSeat2)

//        showtime3.movies = movie3
//        showtime3.cinema = cinema3
//        showtime3.selectedSeats.add(selectedSeat3)

    }

}
