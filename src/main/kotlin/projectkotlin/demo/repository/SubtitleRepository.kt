package projectkotlin.demo.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.demo.entity.Subtitle

interface SubtitleRepository: CrudRepository<Subtitle, Long>