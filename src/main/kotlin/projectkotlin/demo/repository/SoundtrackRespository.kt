package projectkotlin.demo.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.demo.entity.Soundtrack

interface SoundtrackRespository: CrudRepository<Soundtrack, Long>