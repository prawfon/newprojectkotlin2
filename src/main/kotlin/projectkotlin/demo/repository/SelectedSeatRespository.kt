package projectkotlin.demo.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.demo.entity.SelectedSeat

interface SelectedSeatRespository: CrudRepository<SelectedSeat, Long> {
}