package projectkotlin.demo.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.demo.entity.Seats

interface SeatRepository: CrudRepository<Seats, Long>