package projectkotlin.demo.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.demo.entity.SeatInShowTime


interface SeatInShowTimeRepository : CrudRepository<SeatInShowTime, Long> {

}