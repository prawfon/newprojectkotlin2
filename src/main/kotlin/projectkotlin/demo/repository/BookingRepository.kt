package projectkotlin.demo.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.demo.entity.Booking

interface BookingRepository: CrudRepository<Booking, Long>{}