package projectkotlin.demo.repository

import org.springframework.data.repository.CrudRepository
import projectkotlin.demo.entity.Showtime


interface ShowtimeRepository : CrudRepository<Showtime, Long>{
    fun findBycinema_nameContainingIgnoreCase(name:String): List<Showtime>
    fun findBymovies_MovieNameContainingIgnoreCase(name: String): List<Showtime>
}