package projectkotlin.demo.entity

import java.time.chrono.ChronoLocalDateTime
import java.util.*
import javax.persistence.*

@Entity
data class Seats(
        var name:String? = null,
        @Column(name = "seat_rows")
        var row: Int? = null,
        @Column(name = "seat_coloumns")
        var colum: Int? = null,
        var seatstype:SeatType? = null,
        var price: Double)
{
    @Id
    @GeneratedValue
    var id: Long? = null

}