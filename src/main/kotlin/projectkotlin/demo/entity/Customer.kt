package projectkotlin.demo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity

data class Customer(
        override var userName: String? = null,
        override var password: String? = null,
        override var email: String? = null,
        override var firstName:String? = null,
        override var lastName:String? = null,
        var imageUrl: String? = null
): User(userName, password, email, firstName, lastName) {

}

