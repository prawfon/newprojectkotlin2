package projectkotlin.demo.entity.Dto

import projectkotlin.demo.entity.Soundtrack

data class MovieDto(
        var movieName:String? = null,
        var duration:Int?=null,
        var imageUrl: String? = null,
    var soundtrack: List<SoundtrackDto>? = emptyList())
