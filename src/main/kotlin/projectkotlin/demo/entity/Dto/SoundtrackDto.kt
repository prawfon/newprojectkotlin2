package projectkotlin.demo.entity.Dto

data class SoundtrackDto(
        var soundtrack: String? = null
)