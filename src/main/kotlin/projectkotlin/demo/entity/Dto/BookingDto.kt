package projectkotlin.demo.entity.Dto

import java.util.*

data class BookingDto(
        var date: Date
)