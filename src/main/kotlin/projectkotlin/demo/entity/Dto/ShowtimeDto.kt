package projectkotlin.demo.entity.Dto

import projectkotlin.demo.entity.Cinema
import projectkotlin.demo.entity.Movies
import projectkotlin.demo.entity.SelectedSeat
import java.util.*

data class ShowtimeDto (var Startdate: Long? = null,
                        var EndDate: Long? = null,
                        var movies: Movies? = null,
                        var cinema: Cinema? = null,
                        var selectedSeats:List<SelectedSeat>? = null

                        )