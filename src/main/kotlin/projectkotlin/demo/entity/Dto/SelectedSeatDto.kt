package projectkotlin.demo.entity.Dto

data class SelectedSeatDto(
        var horizontal:String? = null,
        var vertical:String? = null,
        var status:Boolean? = null
)