package projectkotlin.demo.entity.Dto

data class SeatsDto(
        var name: String? = null,
        var row: Int? = null,
        var colum: Int? = null,
        var seatstype: String? = null,
        var typeseat: String? = null,
        var price: Double? = null)