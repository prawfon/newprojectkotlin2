package projectkotlin.demo.entity.Dto

data class CustomerDto(
        var userName: String? = null,
        var password: String? =null,
        var email: String? = null,
        var firstName: String? = null,
        var lastName: String? = null,
        var imageUrl: String? = null,
        var id:Long? = null

)