package projectkotlin.demo.entity.Dto

data class UserDto( var username: String? = null,
                    var password: String? = null,
                    var email: String? = null,
                    var firstname: String? = null,
                    var lastname: String? = null,
                    var authorities: List<AuthorityDto> = mutableListOf()
                   )