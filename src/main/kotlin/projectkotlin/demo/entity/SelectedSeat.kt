package projectkotlin.demo.entity

import javax.persistence.*

@Entity
data class SelectedSeat( @ManyToOne
                         var seatDetail: Seats? = null) {

    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToMany
    var seats =  mutableListOf<SeatInShowTime>()

}