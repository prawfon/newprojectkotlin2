package projectkotlin.demo.entity

import projectkotlin.demo.security.entity.JwtUser
import javax.persistence.*
//
//interface User {
//    var userName: String?
//    var password: String?
//    var email: String?
//    var firstName: String?
//    var lastName: String?
//
//}

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
abstract class User(
       open var userName: String? = null,
       open var password: String? = null,
       open var email: String? = null,
       open var firstName: String? = null,
       open var lastName: String? = null

)
{
    @Id
    @GeneratedValue
    var id: Long? = null
    @OneToOne
    var jwtUser:JwtUser? = null
}